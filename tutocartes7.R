## ----setup, include = FALSE----------------------------------------------
require(knitr)
opts_chunk$set(message = FALSE, warning = FALSE, results = 'markup')

## ----tutocarte7-expand---------------------------------------------------
require(rgdal)
require(ggplot2)
require(ggthemes)
jpn0 <- readOGR(dsn = path.expand("~/repos/cartes/jpn/adm/jpn"), layer = "JPN_adm0")
jpn0F <- fortify(jpn0)
xlims <- c(128.5,132.25)
ylims <- c(31,34)
kyushu <- ggplot(data = jpn0F) +
  geom_polygon(aes(x = long, y = lat, group = group), fill = "grey", color = "black", size = .25) +
  coord_fixed(xlim = xlims, ylim = ylims) +
  theme_classic()
kyushu

## ----tutocarte7-noexpand-------------------------------------------------
kyushu <- kyushu +
  scale_x_continuous(expand = c(0,0)) +
  scale_y_continuous(expand = c(0,0))
kyushu

## ----tutocarte7-crop-----------------------------------------------------
require(knitr)
ggsave("kyushu.pdf", kyushu)
plot_crop("kyushu.pdf")

## ----tutocarte7-kyushu---------------------------------------------------
xlims <- c(127.5,132.25)
ylims <- c(30.5,34)
kyushu <- ggplot(data = jpn0F) +
  geom_polygon(aes(x = long, y = lat, group = group), fill = "grey", color = "black", size = .25) +
  coord_fixed(xlim = xlims, ylim = ylims) +
  theme_classic() +
  theme(panel.border = element_rect(fill = NA, colour = 'black', size=0.5)) +
  scale_x_continuous(expand = c(0,0)) +
  scale_y_continuous(expand = c(0,0))
kyushu

## ----tutocarte7-asia-----------------------------------------------------
require(mapdata)
asia <- ggplot() +
  borders("worldHires", fill = "grey", colour = "black", xlim = c(110,150), ylim = c(20,50), size = .25) +
  coord_fixed(xlim = c(110,150), ylim = c(20,50)) +
  theme_map() +
  theme(panel.border = element_rect(fill = NA, colour = 'black', size=0.5), panel.background = element_rect(fill = 'white', colour = NA)) +
  scale_x_continuous(expand = c(0,0)) +
  scale_y_continuous(expand = c(0,0))
asia

## ----tutocarte7-ext------------------------------------------------------
require(raster)
kyushuExt <- as(extent(xlims,ylims), "SpatialPolygons")
kyuAsia <- asia + geom_polygon(data = kyushuExt, aes(x = long, y = lat), fill = NA, color = "black", size = .75)
kyuAsia

## ----tutocarte7-kyushuasia-----------------------------------------------
require(ggmap)
require(grid)
kyushuAsia <- kyushu +  inset(ggplotGrob(kyuAsia), xmin = min(xlims), xmax = 129.5, ymin = min(ylims), ymax = 32)
kyushuAsia

## ----tutocarte7-kyushuasia2----------------------------------------------
require(ggsn)
kyushuAsia + 
  north(data = fortify(kyushuExt), symbol = 3, location = "topleft", scale = .08) + 
  scalebar(data = fortify(kyushuExt), dist = 40, dd2km = TRUE, model = 'WGS84', st.size = 3, st.dist = .035, location = "bottomright", st.bottom = FALSE, anchor = c(x = 132.05, y = 30.6))

## ----tutocarte7-oki------------------------------------------------------
jpn2 <- readOGR(dsn = path.expand("~/repos/cartes/jpn/adm/jpn"), layer = "JPN_adm2")
hondo <- subset(jpn2, NAME_1 != "Okinawa")
oki <- subset(jpn2, NAME_1 == "Okinawa" & !(NAME_2 %in% c("Minami Daito","Unknown5","Unknown8")))

## ----tutocarte7-okipoly--------------------------------------------------
for(i in 1:length(oki@polygons)) {
  for(j in 1:length(oki@polygons[[i]]@Polygons)) {
    oki@polygons[[i]]@Polygons[[j]]@coords[,1] <- oki@polygons[[i]]@Polygons[[j]]@coords[,1] + 6
    oki@polygons[[i]]@Polygons[[j]]@coords[,2] <- oki@polygons[[i]]@Polygons[[j]]@coords[,2] + 17.5
  }
}

## ----tutocarte7-okihondo-------------------------------------------------
jpn <- rbind(hondo, oki)
jpnF <- fortify(jpn)
jpMap <- ggplot() +
  geom_polygon(data = jpnF, aes(x = long, y = lat, group = group), fill = "grey") +
  coord_fixed(xlim = c(min(jpnF$long), 146), ylim = c(27,max(jpnF$lat))) +
  scale_x_continuous(expand = c(0,0)) +
  scale_y_continuous(expand = c(0,0)) +
  theme_classic() +
  theme(panel.border = element_rect(fill = NA, colour = 'black', size=0.5))
jpMap

## ----tutocarte7-okihondo2------------------------------------------------
border <- data.frame(
  long <- c(129, 131, 136, 136),
  lat <- c( 40.5, 40.5, 44, 45)
)
jpMap + 
  geom_path(data = border, aes(x = long, y = lat), color = "black", size = 0.5)

